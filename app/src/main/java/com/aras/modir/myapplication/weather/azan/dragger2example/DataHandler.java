package com.aras.modir.myapplication.weather.azan.dragger2example;

import android.content.Context;
import android.preference.PreferenceManager;

public class DataHandler {

    Context mContext;

    public DataHandler(Context mContext) {
        this.mContext = mContext;
    }

    public void saveData (String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(mContext)
                .edit().putString(key, value).apply();
    }

    public String getValue (String key, String defValue) {
        return PreferenceManager.getDefaultSharedPreferences(mContext)
                .getString(key, defValue);
    }
}
