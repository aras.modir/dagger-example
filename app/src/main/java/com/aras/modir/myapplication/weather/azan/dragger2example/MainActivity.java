package com.aras.modir.myapplication.weather.azan.dragger2example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject
    DataHandler dataHandler;

    @Inject
    StudentDAO studentDAO;

    @Inject
    RestApiHandler restApiHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((MyApplication)getApplication()).getDaggerComponent()
                .inject(this);

        studentDAO.setFamily("hasani");
        studentDAO.setName("ali");
        studentDAO.setMobile("09398876542");
        studentDAO.save();

        restApiHandler.setData("ali");

    }
}
