package com.aras.modir.myapplication.weather.azan.dragger2example;

import android.app.Application;

public class MyApplication extends Application {


    private StudentComponent daggerComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        daggerComponent = DaggerStudentComponent.builder()
                .studentModule(new StudentModule(this))
                .build();
    }

    public StudentComponent getDaggerComponent() {
        return daggerComponent;
    }
}
