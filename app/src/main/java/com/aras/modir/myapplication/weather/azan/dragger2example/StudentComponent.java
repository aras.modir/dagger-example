package com.aras.modir.myapplication.weather.azan.dragger2example;

import dagger.Component;

@Component(modules = {StudentModule.class})
public interface StudentComponent {

    void inject(MainActivity daggerActivity);

}
