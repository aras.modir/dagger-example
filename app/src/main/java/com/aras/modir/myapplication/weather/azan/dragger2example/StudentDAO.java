package com.aras.modir.myapplication.weather.azan.dragger2example;

public class StudentDAO {

    String name;
    String family;
    String mobile;

    DataHandler dataHandler;

    public StudentDAO(DataHandler dataHandler) {
        this.dataHandler = dataHandler;
    }

    public void save() {
        dataHandler.saveData("name", name);
        dataHandler.saveData("family", family);
        dataHandler.saveData("mobile", mobile);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
