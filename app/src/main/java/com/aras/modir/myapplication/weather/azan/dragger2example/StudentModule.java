package com.aras.modir.myapplication.weather.azan.dragger2example;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class StudentModule {

    Context mContext;
    private String code;

    public StudentModule(Context mContext, String code) {
        this.mContext = mContext;
        this.code = code;
    }

    @Provides
    public DataHandler getHandler() {
        return new DataHandler(mContext);
    }

    @Provides
    public StudentDAO getStudent(DataHandler dataHandler) {
        return new StudentDAO(dataHandler);
    }

    @Provides
    public String getCode () {
        return code;
    }

    @Provides
    public RestApiHandler setRest(String code) {
        return new RestApiHandler(code);
    }

}
